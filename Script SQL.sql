-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `marina` DEFAULT CHARACTER SET utf8 ;
USE `marina` ;

-- -----------------------------------------------------
-- Table `mydb`.`morador`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`morador` (
  `codMorador` INT NOT NULL,
  `nome_morador` VARCHAR(45) NULL,
  `telefone_morador` VARCHAR(20) NULL,
  PRIMARY KEY (`codMorador`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`apartamento`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`apartamento` (
  `numero` VARCHAR(2) NOT NULL,
  `qtdQuartos` INT NULL,
  `tipoOcupacao` VARCHAR(45) NULL,
  `nomePropretario` VARCHAR(45) NULL,
  `telefonePropretario` VARCHAR(20) NULL,
  `morador_codMorador` INT NOT NULL,
  PRIMARY KEY (`numero`),
  INDEX `fk_apartamento_morador1_idx` (`morador_codMorador` ASC),
  CONSTRAINT `fk_apartamento_morador1`
    FOREIGN KEY (`morador_codMorador`)
    REFERENCES `mydb`.`morador` (`codMorador`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`condominio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`condominio` (
  `valorTotal` DECIMAL(6,2) NOT NULL,
  `mesReferencia` VARCHAR(10) NOT NULL,
  `morador_codMorador` INT NOT NULL,
  PRIMARY KEY (`mesReferencia`),
  INDEX `fk_condominio_morador1_idx` (`morador_codMorador` ASC),
  CONSTRAINT `fk_condominio_morador1`
    FOREIGN KEY (`morador_codMorador`)
    REFERENCES `mydb`.`morador` (`codMorador`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`despesa`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`despesa` (
  `codDespesa` INT NOT NULL,
  `descricao` VARCHAR(45) NULL,
  `tipo` VARCHAR(45) NULL,
  PRIMARY KEY (`codDespesa`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`itemCondominio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`itemCondominio` (
  `condominio_mesReferencia` VARCHAR(10) NOT NULL,
  `despesa_codDespesa` INT NOT NULL,
  `codItem` INT NOT NULL,
  `valordespesa` DECIMAL(6,2) NOT NULL,
  `datadespesa` DATE NOT NULL,
  INDEX `fk_condominio_has_despesa_despesa1_idx` (`despesa_codDespesa` ASC),
  INDEX `fk_condominio_has_despesa_condominio_idx` (`condominio_mesReferencia` ASC),
  PRIMARY KEY (`codItem`),
  CONSTRAINT `fk_condominio_has_despesa_condominio`
    FOREIGN KEY (`condominio_mesReferencia`)
    REFERENCES `mydb`.`condominio` (`mesReferencia`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_condominio_has_despesa_despesa1`
    FOREIGN KEY (`despesa_codDespesa`)
    REFERENCES `mydb`.`despesa` (`codDespesa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;