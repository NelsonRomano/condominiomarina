package fronteira;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import controle.CondominioControle;
import entidade.Condominio;

public class CondominioForm implements ActionListener {
	private JFrame janela = new JFrame("Gest�o de Condom�nios");
	private JTextField txtCodMorador = new JTextField();
	private JTextField txtValorTotal = new JTextField();
	private JTextField txtMesReferencia = new JTextField();
	private JButton btnAdicionar = new JButton("Adicionar");
	private JButton btnPesquisar = new JButton("Pesquisar");
	private CondominioControle controle = new CondominioControle();

	public CondominioForm() {
		JPanel panPrincipal = new JPanel(new BorderLayout());
		JPanel panForm = new JPanel(new GridLayout(6, 2));

		janela.setContentPane(panPrincipal);
		panPrincipal.add(panForm, BorderLayout.NORTH);

		panForm.add(new JLabel("C�digo do Morador:"));
		panForm.add(txtCodMorador);
		panForm.add(new JLabel("Valor total:"));
		panForm.add(txtValorTotal);
		panForm.add(new JLabel("M�s de refer�ncia:"));
		panForm.add(txtMesReferencia);
		panForm.add(btnAdicionar);
		panForm.add(btnPesquisar);

		btnAdicionar.addActionListener(this);
		btnPesquisar.addActionListener(this);

		janela.setSize(600, 400);
		janela.setVisible(true);
		janela.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	public void CondominioToForm(Condominio a) {
		txtValorTotal.setText(Float.toString(a.getValor_total()));
		txtMesReferencia.setText(a.getMes_referencia());
	}

	public Condominio formToCondominio() {
		Condominio a = new Condominio();
		try {
			a.setValor_total(Float.parseFloat(txtValorTotal.getText()));
			a.setMes_referencia(txtMesReferencia.getText());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return a;
	}

	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if ("Adicionar".equals(cmd)) {
			controle.adicionar(formToCondominio(), Integer.parseInt(txtCodMorador.getText()));
		} else if ("Pesquisar".equals(cmd)) {
			List<Condominio> lista = controle.pesquisar(txtMesReferencia.getText(), Integer.parseInt(txtCodMorador.getText()));
			if (lista.size() > 0) {
				CondominioToForm(lista.get(0));
			}
		}
	}

}