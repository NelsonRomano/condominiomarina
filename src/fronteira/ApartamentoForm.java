package fronteira;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import controle.ApartamentoControle;
import entidade.Apartamento;

public class ApartamentoForm implements ActionListener {
	private JFrame janela = new JFrame("Gest�o de Apartamentos");
	private JTextField txtNumero = new JTextField();	
	private JTextField txtQtdQuartos = new JTextField();
	private JTextField txtTipoOcupacao = new JTextField();
	private JTextField txtNomeProprietario = new JTextField();
	private JTextField txtTelProprietario = new JTextField();
	private JTextField txtCodMorador = new JTextField();
	private JButton btnCadastraMorador = new JButton("Cadastrar Morador...");
	private JButton btnAdicionar = new JButton("Adicionar");
	private JButton btnPesquisar = new JButton("Pesquisar");
	private ApartamentoControle controle = new ApartamentoControle();

	public ApartamentoForm() {
		JPanel panPrincipal = new JPanel(new BorderLayout());
		JPanel panForm = new JPanel(new GridLayout(4, 2));

		janela.setContentPane(panPrincipal);
		panPrincipal.add(panForm, BorderLayout.NORTH);

		panForm.add(new JLabel("N�mero:"));
		panForm.add(txtNumero);
		panForm.add(new JLabel("Qtd. de quartos:"));
		panForm.add(txtQtdQuartos);
		panForm.add(new JLabel("Tipo de ocupa��o:"));
		panForm.add(txtTipoOcupacao);
		panForm.add(new JLabel("Nome do propriet�rio:"));
		panForm.add(txtNomeProprietario);
		panForm.add(new JLabel("Telefone do propriet�rio:"));
		panForm.add(txtTelProprietario);
		panForm.add(new JLabel("C�digo do Morador:"));
		panForm.add(txtCodMorador);
		panForm.add(btnCadastraMorador);
		panForm.add(btnAdicionar);
		panForm.add(btnPesquisar);

		btnAdicionar.addActionListener(this);
		btnPesquisar.addActionListener(this);
		btnCadastraMorador.addActionListener(this);

		janela.setSize(600, 400);
		janela.setVisible(true);
		janela.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	public void ApartamentoToForm(Apartamento a) {
		txtNumero.setText(a.getNumero());
		txtQtdQuartos.setText(Integer.toString(a.getQtdQuartos()));
		txtTipoOcupacao.setText(a.getTipoOcupacao());
		txtNomeProprietario.setText(a.getNomeProprietario());
		txtTelProprietario.setText(a.getTelProprietario());
	}

	public Apartamento formToApartamento() {
		Apartamento a = new Apartamento();

		try {
			a.setNumero(txtNumero.getText());
			a.setQtdQuartos(Integer.parseInt(txtQtdQuartos.getText()));
			a.setTipoOcupacao(txtTipoOcupacao.getText());
			a.setNomeProprietario(txtNomeProprietario.getText());
			a.setTelProprietario(txtTelProprietario.getText());					
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return a;
	}

	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();

		if ("Adicionar".equals(cmd)) {

			controle.adicionar(formToApartamento(), Integer.parseInt(txtCodMorador.getText()));

		} else if ("Pesquisar".equals(cmd)) {
			List<Apartamento> lista = controle.pesquisar(txtNumero.getText());
			if (lista.size() > 0) {
				ApartamentoToForm(lista.get(0));
			}

		} else if ("Cadastrar Morador...".equals(cmd)) {
			new MoradorForm();
		}
	}
}