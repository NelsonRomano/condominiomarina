package fronteira;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import controle.DespesaControle;
import entidade.Despesa;

public class DespesaForm implements ActionListener {
	private JFrame janela = new JFrame("Gest�o de Despesas");
	private JTextField txtCodDespesa = new JTextField();
	private JTextField txtDesc_despesa = new JTextField();
	private JTextField txtTipo_despesa = new JTextField();
	private JButton btnAdicionar = new JButton("Adicionar");
	private JButton btnPesquisar = new JButton("Pesquisar");
	private DespesaControle controle = new DespesaControle();

	public DespesaForm() {
		JPanel panPrincipal = new JPanel(new BorderLayout());
		JPanel panForm = new JPanel(new GridLayout(6, 2));

		janela.setContentPane(panPrincipal);
		panPrincipal.add(panForm, BorderLayout.NORTH);

		panForm.add(new JLabel("C�digo:"));
		panForm.add(txtCodDespesa);
		panForm.add(new JLabel("Descri��o:"));
		panForm.add(txtDesc_despesa);
		panForm.add(new JLabel("Tipo:"));
		panForm.add(txtTipo_despesa);
		panForm.add(btnAdicionar);
		panForm.add(btnPesquisar);

		btnAdicionar.addActionListener(this);
		btnPesquisar.addActionListener(this);

		janela.setSize(600, 400);
		janela.setVisible(true);
		janela.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	public void CondominioToForm(Despesa d) {
		txtCodDespesa.setText(Integer.toString(d.getCodDespesa()));
		txtDesc_despesa.setText(d.getDesc_despesa());
		txtTipo_despesa.setText(d.getTipo_despesa());
	}

	public Despesa formToDespesa() {
		Despesa d = new Despesa();
		try {
			d.setCodDespesa(Integer.parseInt(txtCodDespesa.getText()));
			d.setDesc_despesa(txtDesc_despesa.getText());
			d.setTipo_despesa(txtTipo_despesa.getText());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return d;
	}

	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if ("Adicionar".equals(cmd)) {
			controle.adicionar(formToDespesa());
		} else if ("Pesquisar".equals(cmd)) {
			List<Despesa> lista = controle.pesquisar(Integer.parseInt(txtCodDespesa.getText()));
			if (lista.size() > 0) {
				CondominioToForm(lista.get(0));
			}
		}
	}

}