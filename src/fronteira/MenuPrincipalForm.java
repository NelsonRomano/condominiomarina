package fronteira;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.UnsupportedLookAndFeelException;

public class MenuPrincipalForm implements ActionListener {
	private JFrame janela = new JFrame();
	private JButton btnApartamento = new JButton("Gest�o de Apartamentos");
	private JButton btnCondominio = new JButton("Gest�o de Condom�nio");
	private JButton btnDespesa = new JButton("Gest�o de Despesas");
	private JButton btnItemCondominio = new JButton("Lan�amentos");
	private JButton btnMorador = new JButton("Gest�o de Moradores");

	public MenuPrincipalForm() {
		JPanel panPrincipal = new JPanel();
		JPanel panForm = new JPanel();

		janela.setContentPane(panPrincipal);
		panPrincipal.add(panForm);
		panForm.add(btnApartamento);
		panForm.add(btnCondominio);
		panForm.add(btnDespesa);
		panForm.add(btnItemCondominio);
		panForm.add(btnMorador);

		btnApartamento.addActionListener(this);
		btnCondominio.addActionListener(this);
		btnDespesa.addActionListener(this);
		btnItemCondominio.addActionListener(this);
		btnMorador.addActionListener(this);

		janela.setSize(800, 100);
		janela.setVisible(true);
		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if ("Gest�o de Apartamentos".equals(cmd)) {
			new ApartamentoForm();
		} else if ("Gest�o de Condom�nio".equals(cmd)) {
			new CondominioForm();
		} else if ("Gest�o de Despesas".equals(cmd)) {
			new DespesaForm();
		} else if ("Lan�amentos".equals(cmd)) {
			new ItemCondominioForm();
		} else if ("Gest�o de Moradores".equals(cmd)) {
			new MoradorForm();
		}
	}

	public static void main(String[] args) {
		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		new MenuPrincipalForm();
	}

}