package fronteira;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import controle.MoradorControle;
import entidade.Morador;

public class MoradorForm implements ActionListener {
	private JFrame janela = new JFrame("Gest�o de Moradores");
	private JTextField txtCodMorador = new JTextField();
	private JTextField txtNome_morador = new JTextField();
	private JTextField txtTelefone_morador = new JTextField();
	private JButton btnAdicionar = new JButton("Adicionar");
	private JButton btnPesquisar = new JButton("Pesquisar");
	private MoradorControle controle = new MoradorControle();

	public MoradorForm() {
		JPanel panPrincipal = new JPanel(new BorderLayout());
		JPanel panForm = new JPanel(new GridLayout(6, 2));

		janela.setContentPane(panPrincipal);
		panPrincipal.add(panForm, BorderLayout.NORTH);

		panForm.add(new JLabel("C�digo:"));
		panForm.add(txtCodMorador);
		panForm.add(new JLabel("Nome::"));
		panForm.add(txtNome_morador);
		panForm.add(new JLabel("Telefone:"));
		panForm.add(txtTelefone_morador);

		panForm.add(btnAdicionar);
		panForm.add(btnPesquisar);

		btnAdicionar.addActionListener(this);
		btnPesquisar.addActionListener(this);

		janela.setSize(600, 400);
		janela.setVisible(true);
		janela.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	public void CondominioToForm(Morador m) {
		txtCodMorador.setText(Integer.toString(m.getCodMorador()));
		txtNome_morador.setText(m.getNome_morador());
		txtTelefone_morador.setText(m.getTelefone_morador());
	}

	public Morador formToMorador() {
		Morador ic = new Morador();
		try {
			ic.setCodMorador(Integer.parseInt(txtCodMorador.getText()));
			ic.setNome_morador(txtNome_morador.getText());
			ic.setTelefone_morador(txtTelefone_morador.getText());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		return ic;
	}

	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if ("Adicionar".equals(cmd)) {
			controle.adicionar(formToMorador());
		} else if ("Pesquisar".equals(cmd)) {
			List<Morador> lista = controle.pesquisar(Integer.parseInt(txtCodMorador.getText()));
			if (lista.size() > 0) {
				CondominioToForm(lista.get(0));
			}
		}
	}
	
}