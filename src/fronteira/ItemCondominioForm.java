package fronteira;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import controle.ItemCondominioControle;
import entidade.Condominio;
import entidade.Despesa;
import entidade.ItemCondominio;

public class ItemCondominioForm implements ActionListener {
	private JFrame janela = new JFrame("Lan�amentos");
	private JTextField txtCodItem = new JTextField();
	private JTextField txtValorDespesa = new JTextField();
	private JTextField txtDataDespesa = new JTextField();
	private JTextField txtContaAgua = new JTextField();
	private JTextField txtQtdQuartos = new JTextField();
	private JTextField txtCodDespesa = new JTextField();
	private JTextField txtmesReferencia = new JTextField();
	private JButton btnAdicionar = new JButton("Adicionar");
	private JButton btnPesquisar = new JButton("Pesquisar");
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private ItemCondominioControle controle = new ItemCondominioControle();

	public ItemCondominioForm() {
		JPanel panPrincipal = new JPanel(new BorderLayout());
		JPanel panForm = new JPanel(new GridLayout(4, 2));

		janela.setContentPane(panPrincipal);
		panPrincipal.add(panForm, BorderLayout.NORTH);

		panForm.add(new JLabel("C�digo:"));
		panForm.add(txtCodItem);
		panForm.add(new JLabel("Valor:"));
		panForm.add(txtValorDespesa);
		panForm.add(new JLabel("Data:"));
		panForm.add(txtDataDespesa);
		panForm.add(new JLabel("Conta de �gua:"));
		panForm.add(txtContaAgua);
		panForm.add(new JLabel("Quantidade de quartos:"));
		panForm.add(txtQtdQuartos);
		panForm.add(new JLabel("C�digo do Morador:"));
		panForm.add(txtCodDespesa);
		panForm.add(new JLabel("M�s de refer�ncia do Condom�nio:"));
		panForm.add(txtmesReferencia);
		panForm.add(btnAdicionar);
		panForm.add(btnPesquisar);

		btnAdicionar.addActionListener(this);
		btnPesquisar.addActionListener(this);

		janela.setSize(600, 400);
		janela.setVisible(true);
		janela.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	}

	public void CondominioToForm(ItemCondominio ic) {
		Despesa d = new Despesa();
		Condominio c = new Condominio();
		txtCodItem.setText(Integer.toString(ic.getCodItem()));
		txtValorDespesa.setText(Float.toString(ic.getValorDespesa()));
		txtDataDespesa.setText(sdf.format(ic.getDataDespesa()));
		txtContaAgua.setText(Float.toString(ic.getContaAgua()));
		txtQtdQuartos.setText(Integer.toString(ic.getQtdQuartos()));
		txtCodDespesa.setText(Integer.toString(d.getCodDespesa()));
		txtmesReferencia.setText(c.getMes_referencia());
	}

	public ItemCondominio formToItemCondominio() {
		ItemCondominio ic = new ItemCondominio();
		Condominio c = new Condominio();
		Despesa d = new Despesa();
		try {
			ic.setCodItem(Integer.parseInt(txtCodItem.getText()));
			ic.setValorDespesa(Float.parseFloat(txtValorDespesa.getText()));
			ic.setDataDespesa(sdf.parse(txtDataDespesa.getText()));
			ic.setContaAgua(Float.parseFloat(txtContaAgua.getText()));
			ic.setQtdQuartos(Integer.parseInt(txtQtdQuartos.getText()));
			d.setCodDespesa(Integer.parseInt(txtCodDespesa.getText()));
			c.setMes_referencia(txtmesReferencia.getText());
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return ic;
	}

	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if ("Adicionar".equals(cmd)) {
			controle.adicionar(formToItemCondominio());
		} else if ("Pesquisar".equals(cmd)) {
			List<ItemCondominio> lista = controle.pesquisar(Integer.parseInt(txtCodItem.getText()));
			if (lista.size() > 0) {
				CondominioToForm(lista.get(0));
			}
		}
	}	

}