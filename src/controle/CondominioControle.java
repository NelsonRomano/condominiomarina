package controle;

import java.util.List;
import dao.CondominioDAO;
import dao.CondominioDAOImpl;
import entidade.Condominio;

public class CondominioControle {
	private CondominioDAO aDao = new CondominioDAOImpl();

	public void adicionar(Condominio c, int codMorador) {
		aDao.adicionar(c, codMorador);
	}

	public List<Condominio> pesquisar(String mesReferencia, int codMorador) {
		return aDao.pesquisar(mesReferencia, codMorador);
	}

}