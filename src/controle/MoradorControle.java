package controle;

import java.util.List;
import dao.MoradorDAO;
import dao.MoradorDAOImpl;
import entidade.Morador;

public class MoradorControle {
	private MoradorDAO aDao = new MoradorDAOImpl();

	public void adicionar(Morador m) {
		aDao.adicionar(m);
	}

	public List<Morador> pesquisar(int codMorador) {
		return aDao.pesquisar(codMorador);
	}
	
}