package controle;

import java.util.List;
import dao.DespesaDAO;
import dao.DespesaDAOImpl;
import entidade.Despesa;

public class DespesaControle {
	private DespesaDAO aDAO = new DespesaDAOImpl();

	public void adicionar(Despesa d) {
		aDAO.adicionar(d);
	}

	public List<Despesa> pesquisar(int codDespesa) {
		return aDAO.pesquisar(codDespesa);
	}

}