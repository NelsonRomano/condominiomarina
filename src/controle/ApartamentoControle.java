package controle;

import java.util.List;
import dao.ApartamentoDAO;
import dao.ApartamentoDAOImpl;
import entidade.Apartamento;

public class ApartamentoControle {
	private ApartamentoDAO aDao = new ApartamentoDAOImpl();

	public void adicionar(Apartamento a, int b) {
		aDao.adicionar(a, b);
	}

	public List<Apartamento> pesquisar(String numero) {
		return aDao.pesquisar(numero);
	}
	
}