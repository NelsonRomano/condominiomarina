package controle;

import java.util.List;
import dao.ItemCondominioDAO;
import dao.ItemCondominioDAOImpl;
import entidade.ItemCondominio;

public class ItemCondominioControle {
	private ItemCondominioDAO aDAO = new ItemCondominioDAOImpl();

	public void adicionar(ItemCondominio ic) {
		aDAO.adicionar(ic);
	}

	public List<ItemCondominio> pesquisar(int codItem) {
		return aDAO.pesquisar(codItem);
	}

}