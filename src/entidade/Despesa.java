package entidade;

public class Despesa {
	private int codDespesa;
	private String desc_despesa;
	private String tipo_despesa;
	
	public int getCodDespesa() {
		return codDespesa;
	}
	
	public void setCodDespesa(int codDespesa) {
		this.codDespesa = codDespesa;
	}
	
	public String getDesc_despesa() {
		return desc_despesa;
	}
	
	public void setDesc_despesa(String desc_despesa) {
		this.desc_despesa = desc_despesa;
	}
	
	public String getTipo_despesa() {
		return tipo_despesa;
	}
	
	public void setTipo_despesa(String tipo_despesa) {
		this.tipo_despesa = tipo_despesa;
	}
	
}