package entidade;

public class Condominio {
	private float valor_total;
	private String mes_referencia;
	
	public float getValor_total() {
		return valor_total;
	}
	
	public void setValor_total(float valor_total) {
		this.valor_total = valor_total;
	}
	
	public String getMes_referencia() {
		return mes_referencia;
	}
	
	public void setMes_referencia(String mes_referencia) {
		this.mes_referencia = mes_referencia;
	}
	
}