package entidade;

import java.util.Date;

public class ItemCondominio {
	private int codItem;
	private float valorDespesa;
	private Date dataDespesa;
	private float contaAgua;
	private int qtdQuartos;
	
	public float calculaAgua() {
		float valorAgua;
		valorAgua = (contaAgua/10) * qtdQuartos;
		return valorAgua;
	}

	public int getCodItem() {
		return codItem;
	}

	public void setCodItem(int codItem) {
		this.codItem = codItem;
	}

	public float getValorDespesa() {
		return valorDespesa;
	}

	public void setValorDespesa(float valorDespesa) {
		this.valorDespesa = valorDespesa;
	}

	public Date getDataDespesa() {
		return dataDespesa;
	}

	public void setDataDespesa(Date dataDespesa) {
		this.dataDespesa = dataDespesa;
	}

	public float getContaAgua() {
		return contaAgua;
	}

	public void setContaAgua(float contaAgua) {
		this.contaAgua = contaAgua;
	}

	public int getQtdQuartos() {
		return qtdQuartos;
	}

	public void setQtdQuartos(int qtdQuartos) {
		this.qtdQuartos = qtdQuartos;
	}

}