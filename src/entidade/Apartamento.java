package entidade;

public class Apartamento {
	private String numero;
	private int qtdQuartos;
	private String tipoOcupacao;
	private String nomeProprietario;
	private String telProprietario;
	
	public String getNumero() {
		return numero;
	}
	
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public int getQtdQuartos() {
		return qtdQuartos;
	}
	
	public void setQtdQuartos(int qtdQuartos) {
		this.qtdQuartos = qtdQuartos;
	}
	
	public String getTipoOcupacao() {
		return tipoOcupacao;
	}
	
	public void setTipoOcupacao(String tipoOcupacao) {
		this.tipoOcupacao = tipoOcupacao;
	}
	
	public String getNomeProprietario() {
		return nomeProprietario;
	}
	
	public void setNomeProprietario(String nomeProprietario) {
		this.nomeProprietario = nomeProprietario;
	}
	
	public String getTelProprietario() {
		return telProprietario;
	}
	
	public void setTelProprietario(String telProprietario) {
		this.telProprietario = telProprietario;
	}	
	
}
