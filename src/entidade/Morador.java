package entidade;

public class Morador {
	private int codMorador;
	private String nome_morador;
	private String telefone_morador;
	
	public int getCodMorador() {
		return codMorador;
	}
	
	public void setCodMorador(int codMorador) {
		this.codMorador = codMorador;
	}
	
	public String getNome_morador() {
		return nome_morador;
	}
	
	public void setNome_morador(String nome_morador) {
		this.nome_morador = nome_morador;
	}
	
	public String getTelefone_morador() {
		return telefone_morador;
	}
	
	public void setTelefone_morador(String telefone_morador) {
		this.telefone_morador = telefone_morador;
	}
	
}