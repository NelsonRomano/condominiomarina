package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entidade.Apartamento;

public class ApartamentoDAOImpl implements ApartamentoDAO {
	@Override
	public void adicionar(Apartamento a, int b) {
		Connection con = DBUtil.getInstancia().openConnection();
		String sql = "INSERT INTO apartamento (numero, qtdQuartos, tipoOcupacao, nomePropretario, telefonePropretario, morador_codMorador) "
				+ " VALUES (?, ?, ?, ?, ?, ?) ";
		try {
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, a.getNumero());
			st.setInt(2, a.getQtdQuartos());
			st.setString(3, a.getTipoOcupacao());
			st.setString(4, a.getNomeProprietario());
			st.setString(5, a.getTelProprietario());
			st.setInt(6, b);
			st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBUtil.getInstancia().closeConnection();
	}

	@Override
	public List<Apartamento> pesquisar(String numero) {
		List<Apartamento> lista = new ArrayList<Apartamento>();
		Connection con = DBUtil.getInstancia().openConnection();
		String sql = "SELECT * FROM apartamento WHERE numero like ? ";
		try {
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, numero);

			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				Apartamento a = new Apartamento();
				a.setNumero(rs.getString("numero"));
				a.setQtdQuartos(rs.getInt("qtdQuartos"));
				a.setTipoOcupacao(rs.getString("tipoOcupacao"));
				a.setNomeProprietario(rs.getString("nomePropretario"));
				a.setTelProprietario(rs.getString("telefonePropretario"));
				lista.add(a);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBUtil.getInstancia().closeConnection();
		return lista;
	}
	
}