package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import entidade.Despesa;

public class DespesaDAOImpl implements DespesaDAO {

	@Override
	public void adicionar(Despesa d) {
		Connection con = DBUtil.getInstancia().openConnection();
		String sql = "INSERT INTO despesa (codDespesa, descricao, tipo) VALUES (?, ?, ?);";
		try {
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, d.getCodDespesa());
			st.setString(2, d.getDesc_despesa());
			st.setString(3, d.getTipo_despesa());
			st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBUtil.getInstancia().closeConnection();
	}

	@Override
	public List<Despesa> pesquisar(int codDespesa) {
		List<Despesa> lista = new ArrayList<Despesa>();
		Connection con = DBUtil.getInstancia().openConnection();
		String sql = "SELECT * FROM despesa WHERE codDespesa like ?;";
		try {
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, codDespesa);

			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				Despesa d = new Despesa();
				d.setCodDespesa(rs.getInt("codDespesa"));
				d.setDesc_despesa(rs.getString("descricao"));
				d.setTipo_despesa(rs.getString("tipo"));
				lista.add(d);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBUtil.getInstancia().closeConnection();
		return lista;
	}

}