package dao;

import java.util.List;
import entidade.Morador;

public interface MoradorDAO {
	public void adicionar(Morador m);
	public List<Morador> pesquisar(int codMorador);
}