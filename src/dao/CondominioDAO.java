package dao;

import java.util.List;
import entidade.Condominio;

public interface CondominioDAO {
	public void adicionar(Condominio c, int codMorador);
	public List<Condominio> pesquisar(String mesReferencia, int codMorador);
}