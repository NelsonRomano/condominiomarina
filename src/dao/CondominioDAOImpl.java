package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import entidade.Condominio;

public class CondominioDAOImpl implements CondominioDAO {
	
	@Override
	public void adicionar(Condominio c, int codMorador) {
		Connection con = DBUtil.getInstancia().openConnection();
		String sql = "INSERT INTO condominio (valorTotal, mesReferencia, morador_codMorador) VALUES (?, ?, ?);";
		try {
			PreparedStatement st = con.prepareStatement(sql);
			st.setFloat(1, c.getValor_total());
			st.setString(2, c.getMes_referencia());
			st.setInt(3, codMorador);
			st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBUtil.getInstancia().closeConnection();
	}

	@Override
	public List<Condominio> pesquisar(String mesReferencia, int codMorador) {
		List<Condominio> lista = new ArrayList<Condominio>();
		Connection con = DBUtil.getInstancia().openConnection();
		String sql = "SELECT * FROM condominio WHERE mesReferencia like ? and morador_codMorador like ?;";
		try {
			PreparedStatement st = con.prepareStatement(sql);
			st.setString(1, mesReferencia);
			st.setInt(2, codMorador);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				Condominio c = new Condominio();
				c.setValor_total(rs.getLong("valorTotal"));
				c.setMes_referencia(rs.getString("mesReferencia"));
				lista.add(c);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBUtil.getInstancia().closeConnection();
		return lista;
	}
}