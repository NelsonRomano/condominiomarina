package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entidade.Condominio;
import entidade.Despesa;
import entidade.ItemCondominio;

public class ItemCondominioDAOImpl implements ItemCondominioDAO {

	@Override
	public void adicionar(ItemCondominio ic) {
		Condominio c = new Condominio();
		Despesa d = new Despesa();
		Connection con = DBUtil.getInstancia().openConnection();
		String sql = "INSERT INTO itemCondominio (codItem, valordespesa, datadespesa, condominio_mesReferencia, despesa_codDespesa) "
				+ " VALUES (?, ?, ?, ?, ?) ";
		try {
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, ic.getCodItem());
			st.setFloat(2, ic.getValorDespesa());			
			java.sql.Date data = new java.sql.Date(ic.getDataDespesa().getTime());
			st.setDate(3, data);
			st.setString(4, c.getMes_referencia());
			st.setInt(5, d.getCodDespesa());
			st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBUtil.getInstancia().closeConnection();
	}

	@Override
	public List<ItemCondominio> pesquisar(int codItem) {
		Condominio c = new Condominio();
		Despesa d = new Despesa();
		List<ItemCondominio> lista = new ArrayList<ItemCondominio>();
		Connection con = DBUtil.getInstancia().openConnection();
		String sql = "SELECT * FROM itemCondominio WHERE codItem like ? ";
		try {
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, codItem);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				ItemCondominio ic = new ItemCondominio();
				ic.setCodItem(rs.getInt("codItem"));
				ic.setValorDespesa(rs.getFloat("valordespesa"));
				ic.setDataDespesa(rs.getDate("datadespesa"));
				c.setMes_referencia(rs.getString("condominio_mesReferencia"));
				d.setCodDespesa(rs.getInt("despesa_codDespesa"));
				lista.add(ic);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBUtil.getInstancia().closeConnection();
		return lista;
	}

}