package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import entidade.Morador;

public class MoradorDAOImpl implements MoradorDAO {
	@Override
	public void adicionar(Morador m) {
		Connection con = DBUtil.getInstancia().openConnection();
		String sql = "INSERT INTO morador (codMorador, nome_morador, telefone_morador) "
				+ " VALUES (?, ?, ?) ";
		try {
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, m.getCodMorador());
			st.setString(2, m.getNome_morador());
			st.setString(3, m.getTelefone_morador());
			st.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBUtil.getInstancia().closeConnection();
	}

	@Override
	public List<Morador> pesquisar(int codMorador) {
		List<Morador> lista = new ArrayList<Morador>();
		Connection con = DBUtil.getInstancia().openConnection();
		String sql = "SELECT * FROM morador WHERE codMorador like ? ";
		try {
			PreparedStatement st = con.prepareStatement(sql);
			st.setInt(1, codMorador);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				Morador m = new Morador();
				m.setCodMorador(rs.getInt("codMorador"));
				m.setNome_morador(rs.getString("nome_morador"));
				m.setTelefone_morador(rs.getString("telefone_morador"));
				lista.add(m);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBUtil.getInstancia().closeConnection();
		return lista;
	}

}