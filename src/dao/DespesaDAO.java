package dao;

import java.util.List;
import entidade.Despesa;

public interface DespesaDAO {
	public void adicionar(Despesa d);
	public List<Despesa> pesquisar(int codDespesa);
}