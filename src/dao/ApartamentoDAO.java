package dao;

import java.util.List;
import entidade.Apartamento;

public interface ApartamentoDAO {
	public void adicionar(Apartamento a, int b);
	public List<Apartamento> pesquisar(String numero);
}