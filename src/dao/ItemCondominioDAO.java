package dao;

import java.util.List;
import entidade.ItemCondominio;

public interface ItemCondominioDAO {
	public void adicionar(ItemCondominio ic);
	public List<ItemCondominio> pesquisar(int codItem);
}